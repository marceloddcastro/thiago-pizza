var PriceCalculator = (function () {
    function PriceCalculator() {

    }

    return PriceCalculator;
});

PriceCalculator.calcTotal = function (order) {

    // Calcular o total do pedido
    for (let pizza of order.pizzas) {
        order.total += pizza.total;
    }

    order.tax = order.total * 0.12;
    order.total = order.total + order.tax;
    
    return order
}

module.exports = PriceCalculator;
