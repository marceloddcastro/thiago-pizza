const seeder = require('mongoose-seed');
const urlConnection = 'mongodb://localhost/marceloC';
// Connect to MongoDB via Mongoose 
seeder.connect(urlConnection, function () {

    // Load Mongoose models 
    seeder.loadModels([
        './models/pizza.js',
        './models/topping.js',
        './models/order.js'
    ]);

    // Clear specified collections 
    seeder.clearModels(['Pizza', 'Topping'], function () {

        // Callback to populate DB once collections have been cleared 
        seeder.populateModels(data, function () {
            //seeder.disconnect(); 
        });

    });
});

// Data array containing seed data - documents organized by Model 
var data = [
    {
        model: "Pizza",
        documents: [
            {
                description: "Margherita",
                sizes: [{
                    description: "Small",
                    price: 10.50
                },
                    {
                        description: "Medium",
                        price: 12.50
                    },
                    {
                        description: "Large",
                        price: 15.50
                    }]
            },
            {
                description: "Prosciutto",
                sizes: [{
                    description: "Small",
                    price: 10.50
                },
                    {
                        description: "Medium",
                        price: 12.50
                    },
                    {
                        description: "Large",
                        price: 15.50
                    }]
            },
            {
                description: "Funghi",
                sizes: [{
                    description: "Small",
                    price: 10.50
                },
                    {
                        description: "Medium",
                        price: 12.50
                    },
                    {
                        description: "Large",
                        price: 15.50
                    }]
            },
            {
                description: "Hawaiian",
                sizes: [{
                    description: "Small",
                    price: 10.50
                },
                    {
                        description: "Medium",
                        price: 12.50
                    },
                    {
                        description: "Large",
                        price: 15.50
                    }]
            },
            {
                description: "Vegetarian",
                sizes: [{
                    description: "Small",
                    price: 10.50
                },
                    {
                        description: "Medium",
                        price: 12.50
                    },
                    {
                        description: "Large",
                        price: 15.50
                    }]
            }
        ]
    },
    {
        model: 'Topping',
        documents: [
            {
                description: 'Mushrooms',
                price: 2.30
            },
            {
                description: 'Garlic',
                price: 1.50
            },
            {
                description: 'Peppers',
                price: 3.25
            },
            {
                description: 'Parmesan Cheese',
                price: 0.98
            },
            {
                description: 'Bacon',
                price: 50.00
            },
            {
                description: 'Tomatoes',
                price: 2.90
            },
            {
                description: 'Olives',
                price: 2.10
            }
        ]
    }
];