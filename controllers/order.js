const Order = require('../models').Order;
const priceCalc = require('../PriceCalculator');
module.exports = function (app) {

    this.createOrder = function (req, res) {
        console.log(req.body);

        let __newOrder = priceCalc.calcTotal(req.body);

        let newOrder = Order(__newOrder);

        newOrder.save()
            .then(result => {
                res.status(200).json('New order created with success!');
            }).catch(err => res.status(412).json(err.message));
    }

    this.getOrders = function (req, res) {
        Order.find({})
            .then(result => res.status(200).json(result))
            .catch(err => res.status(412).json(err.message));
    }

    this.searchOrder = function (req, res) {

        let or = [];
        if (req.body.address && req.body.address != "")
            or.push({'client.address': {$regex: new RegExp(req.body.address, "ig")}});
        if (req.body.phone && req.body.phone != "")
            or.push({'client.telephone': {$regex: new RegExp(req.body.phone, "ig")}});

        Order.find({
            $or: or
        })
            .then(result => res.status(200).json(result))
            .catch(err => res.status(412).json(err.message));

    }

    return this;
}