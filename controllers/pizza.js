const Pizza = require('../models').Pizza;
const Topping = require('../models').Topping;

module.exports = function (app) {

    this.getPizzas = function (req, res) {
        Pizza.find({})
            .then(result => res.status(200).json(result))
            .catch(err => res.status(412).json(err.message));
    }

    this.getToppings = function (req, res) {
        Topping.find({})
            .then(result => res.status(200).json(result))
            .catch(err => res.status(412).json(err.message));
    }

    return this;
}