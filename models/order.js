const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var OrderSchema = new Schema({
    client: {
        name: { type: String, required: [true, 'Name required!'] },
        email: { type: String, required: [true, 'Email required!'] },
        address: { type: String, required: [true, 'Address required!'] },
        telephone: { type: String, required: [true, 'Telephone required!'] },
    },
    pizzas: [
        {
            description: { type: String, required: [true, 'Pizza description required!'] },
            price: { type: Number, required: [true, 'Pizza price required!'] },
            qty: { type: Number, required: [true, 'Pizza quantity required!'] },
            subTotal: Number,
            toppings: [{
                description: String,
                price: Number
            }],
            total: Number
        }
    ],
    tax: Number,
    total: Number
});

module.exports = mongoose.model('Order', OrderSchema);