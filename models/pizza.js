const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var PizzaSchema = new Schema({
  description: { type: String, required: true },
  sizes: [{
      description: String,
      price: Number
  }]
});

module.exports = mongoose.model('Pizza', PizzaSchema);