const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var ToppingSchema = new Schema({
  description: { type: String, required: true },
  price: { type: Number, required: true }
});

module.exports = mongoose.model('Topping', ToppingSchema);