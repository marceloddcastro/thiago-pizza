const apiRouter = require('express').Router();

module.exports = function (app) {
    apiRouter.get('/orders', app.controllers.order.getOrders);
    apiRouter.post('/searchOrder', app.controllers.order.searchOrder);
    apiRouter.post('/order', app.controllers.order.createOrder);

    app.use('/api', apiRouter);
}