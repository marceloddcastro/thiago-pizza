const apiRouter = require('express').Router();

module.exports = function (app) {
    apiRouter.get('/pizzas', app.controllers.pizza.getPizzas);
    apiRouter.get('/toppings', app.controllers.pizza.getToppings);
    app.use('/api', apiRouter);
}